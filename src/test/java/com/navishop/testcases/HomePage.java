package com.navishop.testcases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.navishop.functionlibrary.CommonMethods;
import com.navishop.utils.ExcelReader;
import com.navishop.utils.TestUtil;
import com.navishop.utils.TestsListenerAdapter;


public class HomePage {
	
	String filename=System.getProperty("user.dir")+"\\src\\test\\java\\com\\navishop\\data\\NaviShop.xlsx";
	ExcelReader xls = new ExcelReader(filename);
	CommonMethods commonMethods=CommonMethods.getCommanMethodsInstance();
	
	
/*	@BeforeTest()
	public void initializeBrowserInstance(){
		commonMethods.openBrowser("testBrowser");
	}*/
	
	@Test(dataProvider="getTestData")
public void HomePageVerfication(Hashtable<String,String> data) throws IOException, InterruptedException{
		
		
		if(!TestUtil.isTestCaseExecutable("TC02", xls))
			throw new SkipException("Skipping the test as runmode is set to no");
			      if(!data.get("RunMode").equals("Y"))
			      throw new SkipException("Skipping the test as data set runmode is no");
			
			CommonMethods commonMethods=CommonMethods.getCommanMethodsInstance();
			
			commonMethods.executeKeywords("TC02",data);
			
	}
	@DataProvider
	public Object[][] getTestData(){
		return TestUtil.getData("CheckHomePage",xls);
	}
}