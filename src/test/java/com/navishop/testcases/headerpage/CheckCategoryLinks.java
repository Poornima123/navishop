package com.navishop.testcases.headerpage;

import java.io.IOException;
import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.navishop.functionlibrary.CommonMethods;
import com.navishop.utils.ExcelReader;
import com.navishop.utils.TestUtil;

public class CheckCategoryLinks {
	String filename = System.getProperty("user.dir")
			+ "\\src\\test\\java\\com\\navishop\\data\\NaviShop.xlsx";
	ExcelReader xls = new ExcelReader(filename);
	CommonMethods commonMethods = CommonMethods.getCommanMethodsInstance();

	@BeforeTest()
	public void initializeBrowserInstance() {
		commonMethods.openBrowser("firefoxTestBrowser");
	}

	@Test(dataProvider = "getCategoryData")
	// @Test()
	public void checkCategoryLinks(Hashtable<String, String> data)
			throws IOException, InterruptedException {

		if (!TestUtil.isTestCaseExecutable("TC_HeaderPage_001", xls))
			throw new SkipException("Skipping the test as runmode is set to no");
		if (!data.get("RunMode").equals("Y"))
			throw new SkipException(
					"Skipping the test as data set runmode is no");
		commonMethods.executeKeywords("TC_HeaderPage_001", data);

	}

	@DataProvider
	public Object[][] getCategoryData() {
		return TestUtil.getData("CheckCategoryLink", xls);
	}

	@AfterTest
	public void closeBrowserInstance() {
		commonMethods.closeBrowser();
	}

}
