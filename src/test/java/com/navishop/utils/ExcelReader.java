package com.navishop.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ExcelReader {
	
		public static String filename = System.getProperty("user.dir")+"\\src\\test\\java\\com\\navishop\\data\\NaviShop.xlsx";
		public  String path;
		public  FileInputStream fileInputStream = null;
		public  FileOutputStream fileOutputStream =null;
		private XSSFWorkbook workbook = null;
		private XSSFSheet sheet = null;
		private XSSFRow row   =null;
		private XSSFCell cell = null;
		
		public ExcelReader(String path) {
			
			this.path=path;
			try {
				fileInputStream = new FileInputStream(path);
				workbook = new XSSFWorkbook(fileInputStream);
				sheet = workbook.getSheetAt(0);
				fileInputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			} 
			
		}
		
		
		/*public static void main(String arg[]) throws IOException{
			ExcelReader datatable = null;
				 datatable = new ExcelReader("H:\\Student_Selenium_Workspaces\\Framework_Weekend\\src\\Framework_XL_Files\\Controller.xlsx");
					for(int col=0 ;col< datatable.getColumnCount("TC5"); col++){
						System.out.println(datatable.getCellData("TC5", col, 1));
					}
		}*/
		
		// Returns number of Rows in the excel sheet
		public int getRowCount(String sheetName){
			int index = workbook.getSheetIndex(sheetName);
			if(index==-1)
				return 0;
			else{
			sheet = workbook.getSheetAt(index);
			int number=sheet.getLastRowNum()+1;
			return number;
			}
			
		}
		
		
		//Returns number of Columns in the excel sheet
		public int getColumnCount(String sheetName){
			if(!isSheetExist(sheetName))
			 return -1;
			sheet = workbook.getSheet(sheetName);
			row = sheet.getRow(0);
			if(row==null)
				return -1;
			return row.getLastCellNum();
		}
		
		
		// Returns String data from the the Cell
		public String getCellData(String sheetName,String colName,int rowNum){
			try{
				if(rowNum <=0)
					return "";

				int index = workbook.getSheetIndex(sheetName);
				int col_Num=-1;
				if(index==-1)
					return "";

				sheet = workbook.getSheetAt(index);
				row=sheet.getRow(0);
				for(int i=0;i<row.getLastCellNum();i++){
					if(row.getCell(i).getStringCellValue().trim().equals(colName.trim()))
						col_Num=i;
				}
				if(col_Num==-1)
					return "";

				sheet = workbook.getSheetAt(index);
				row = sheet.getRow(rowNum-1);
				if(row==null)
					return "";
				cell = row.getCell(col_Num);

				if(cell==null)
					return "";
				if(cell.getCellType()==Cell.CELL_TYPE_STRING)
					return cell.getStringCellValue();
				else if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC || cell.getCellType()==Cell.CELL_TYPE_FORMULA ){

					String cellText  = String.valueOf(cell.getNumericCellValue());
					if (HSSFDateUtil.isCellDateFormatted(cell)) {
						// format in form of M/D/YY
						double d = cell.getNumericCellValue();

						Calendar cal =Calendar.getInstance();
						cal.setTime(HSSFDateUtil.getJavaDate(d));
						cellText =
								(String.valueOf(cal.get(Calendar.YEAR))).substring(2);
						cellText = cal.get(Calendar.DAY_OF_MONTH) + "/" +
								cal.get(Calendar.MONTH)+1 + "/" + 
								cellText;
					}
					return cellText;
				}else if(cell.getCellType()==Cell.CELL_TYPE_BLANK)
					return ""; 
				else 
					return String.valueOf(cell.getBooleanCellValue());
			}
			catch(Exception e){
				e.printStackTrace();
				return "row "+rowNum+" or column "+colName +" does not exist in xls";
			}
		}
		
		
		
		public String getCellData(String sheetName,int colNum,int rowNum){
			try{
				if(rowNum <=0)
					return "";
				int index = workbook.getSheetIndex(sheetName);
				if(index==-1)
					return "";
				sheet = workbook.getSheetAt(index);
				row = sheet.getRow(rowNum-1);
				if(row==null)
					return "";
				cell = row.getCell(colNum);
				if(cell==null)
					return "";

				if(cell.getCellType()==Cell.CELL_TYPE_STRING)
					return cell.getStringCellValue();
				else if(cell.getCellType()==Cell.CELL_TYPE_NUMERIC || cell.getCellType()==Cell.CELL_TYPE_FORMULA ){
					String cellText  = String.valueOf(cell.getNumericCellValue());
					if (HSSFDateUtil.isCellDateFormatted(cell)) {
						// format in form of M/D/YY
						double d = cell.getNumericCellValue();

						Calendar cal =Calendar.getInstance();
						cal.setTime(HSSFDateUtil.getJavaDate(d));
						cellText =
								(String.valueOf(cal.get(Calendar.YEAR))).substring(2);
						cellText = cal.get(Calendar.MONTH)+1 + "/" +
								cal.get(Calendar.DAY_OF_MONTH) + "/" +
								cellText;
					}
					return cellText;
				}else if(cell.getCellType()==Cell.CELL_TYPE_BLANK)
					return "";
				else 
					return String.valueOf(cell.getBooleanCellValue());
			}
			catch(Exception e){
				e.printStackTrace();
				return "row "+rowNum+" or column "+colNum +" does not exist  in xls";
			}
		}

		
		
		public boolean setCellData(String sheetName,String colName,int rowNum, String data){
			try{
			fileInputStream = new FileInputStream(path); 
			workbook = new XSSFWorkbook(fileInputStream);

			if(rowNum<=0)
				return false;
			
			int index = workbook.getSheetIndex(sheetName);
			int colNum=-1;
			if(index==-1)
				return false;
			
			
			sheet = workbook.getSheetAt(index);
			

			row=sheet.getRow(0);
			for(int i=0;i<row.getLastCellNum();i++){
				if(row.getCell(i).getStringCellValue().trim().equals(colName))
					colNum=i;
			}
			if(colNum==-1)
				return false;

			sheet.autoSizeColumn(colNum); 
			row = sheet.getRow(rowNum-1);
			if (row == null)
				row = sheet.createRow(rowNum-1);
			
			cell = row.getCell(colNum);	
			if (cell == null)
		        cell = row.createCell(colNum);

		    cell.setCellValue(data);

		    fileOutputStream = new FileOutputStream(path);

			workbook.write(fileOutputStream);

			fileOutputStream.close();	

			}
			catch(Exception e){
				e.printStackTrace();
				return false;
			}
			return true;
		}
		
		
		// Checks the existence of the excel sheet and returns a boolean value.
		public boolean isSheetExist(String sheetName){
			int index = workbook.getSheetIndex(sheetName);
			if(index==-1){
				index=workbook.getSheetIndex(sheetName.toUpperCase());
					if(index==-1)
						return false;
					else
						return true;
			}
			else
				return true;
		}
		

}
