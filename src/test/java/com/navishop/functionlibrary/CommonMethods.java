package com.navishop.functionlibrary;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


//import com.flyhi.website.utils.Event;
import com.navishop.utils.ExcelReader;

public class CommonMethods {

	public static ExcelReader xls = new ExcelReader(
			System.getProperty("user.dir")
					+ "\\src\\test\\java\\com\\navishop\\data\\NaviShop.xlsx");
	public static Properties config = null;
	public static Properties Objects = null;
	public static WebDriver driver = null;
	static CommonMethods event = null;

	public CommonMethods() {
		System.out.println("Initializing common methods");
		try {
			config = new Properties();
			FileInputStream fs;

			fs = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\com\\navishop\\data\\config.properties");
			config.load(fs);
			Objects=new Properties();
			fs=new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\com\\navishop\\data\\Objects.properties");

			Objects.load(fs);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static CommonMethods getCommanMethodsInstance() {
		if (event == null) {
			event = new CommonMethods();
		}

		return event;
	}

	public ArrayList<String> executeKeywords(String Testname,
			Hashtable<String, String> data) throws IOException,
			InterruptedException {
		System.out.println("Executing Keywords" + Testname);
		String Keyword = null;
		String ObjectKey = null;
		String dataColVal = null;
		String objectParams = null;

		ArrayList<String> Resultset = new ArrayList<String>();
		for (int rNum = 2; rNum <= xls.getRowCount("Test Steps"); rNum++) {
			if (Testname.equals(xls.getCellData("Test Steps", "TCID", rNum))) {
				Keyword = xls.getCellData("Test Steps", "Keyword", rNum);
				ObjectKey = xls.getCellData("Test Steps", "Object", rNum);
				objectParams = xls.getCellData("Test Steps",
						"Object Parameters", rNum);
				dataColVal = xls.getCellData("Test Steps", "Data", rNum);

				String Result = null;
				// boolean b=true;
				if (Keyword.equals("openBrowser"))
					Result = openBrowser(dataColVal);
				else if (Keyword.equals("wait"))
					Result = implicitWait();
				else if (Keyword.equals("navigate"))
					Result = navigate(dataColVal);
				else if (Keyword.equals("click"))
					Result = click(ObjectKey);
				else if (Keyword.equals("IsElementPresent"))
					Result = IsElementPresent(ObjectKey);
				else if (Keyword.equals("getCurrentUrl"))
					Result = getCurrentUrl();
				else if (Keyword.equals("getTitle"))
					Result = getTitle();
				else if (Keyword.equals("Onhover"))
					Result = hover(ObjectKey);
				else if (Keyword.equals("assertText"))
					Result = assertText(ObjectKey,objectParams,dataColVal,data);
				else if (Keyword.equals("clickText"))
					Result = clickText(ObjectKey,objectParams,dataColVal,data);
				else if (Keyword.equals("closeBrowser"))
					Result = closeBrowser();
				else if(Keyword.equals("SelectRandomElement"))
					Result=SelectRandomElement(ObjectKey);
			   else if(Keyword.equals("ScrollBy"))
				      Result=Scrollby();
			   else if(Keyword.equals("ExplictiWait"))
					Result=ExplictiWait(ObjectKey);
				System.out.println(Result);
				Resultset.add(Result);
				Assert.assertEquals(Result, "pass");
			}
		}
		return Resultset;
	}
	public  String SelectRandomElement(String xpathKey){ 
		WebElement myElement =driver.findElement(By.xpath(Objects.getProperty(xpathKey)));
		Actions builder = new Actions(driver); 
		Actions moveToElement = builder.moveToElement(myElement);
		moveToElement.perform();
		return "pass";
		}
		
		
		public String Scrollby()
		{
 
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,500)", "");
			return "pass";
		}
		
		private String ExplictiWait(String xpathKey) {
			WebDriverWait wait = new WebDriverWait(driver, 2);
			 
			WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Objects.getProperty(xpathKey))));
			return "pass";
		
	}

	public String assertText(String objectKey, String objectParams,
			String dataColVal, Hashtable<String, String> data) {
		System.out.println("Executing assert text method");
		// resolve the parameterized xpath
		String resolveXpath = resolveXpath(objectKey, objectParams, data);
		
		 String actualValue = driver.findElement(
				By.xpath(resolveXpath)).getText().trim();
		if(actualValue.isEmpty())
		{
			clickOnCategoryNavigationArrow();
			 actualValue = driver.findElement(
					By.xpath(resolveXpath)).getText();
		}
		//String dataValue=data.get(dataColVal).trim();
		if (!data.get(dataColVal).equalsIgnoreCase(actualValue)) {
		//if(!dataValue.equalsIgnoreCase(actualValue)){
			return " Test Assertion failed";
		}
		
		return "pass";
		
	}

	public String clickText(String objectKey, String objectParams,String dataColVal,
			Hashtable<String, String> data) {
		System.out.println("Executing click on text method");
		String resolveXpath = resolveXpath(objectKey, objectParams, data);
		driver.findElement(By.xpath(resolveXpath)).click();
		String actualPageTitle=getTitle();
		if(!data.get(dataColVal).equalsIgnoreCase(actualPageTitle)){
			return "Page Title Assertion Failed";
		}
		return "pass";
	}
	
	public String hoverOnElement(String objectKey, String objectParams,
			Hashtable<String, String> data) {
		String resolveXpath = resolveXpath(objectKey, objectParams, data);
		WebElement element=driver.findElement(By.xpath(resolveXpath));
		Actions builder = new Actions(driver);
		Actions hoverOverRegistrar = builder.moveToElement(element);
		hoverOverRegistrar.perform();
		;
		return "pass";
	}

	private String resolveXpath(String objectKey, String objectParams,
			Hashtable<String, String> data) {
		if (StringUtils.isEmpty(objectParams)) {
			//return objectKey;
			return String.format(Objects.getProperty(objectKey));
		}

		String[] dataTokens = objectParams.split(",");
		for (int index = 0; index < dataTokens.length; index++) {
			String token = dataTokens[index];
			String tokenValue = data.get(token);
			dataTokens[index] = String.valueOf(parseToInt(tokenValue));
		}
		return String.format(Objects.getProperty(objectKey), dataTokens);

	}

	public String hover(String xpathKey) {
		System.out.println("Executing onhover");
		try {
			WebElement myElement = driver.findElement(By.xpath(Objects
					.getProperty(xpathKey)));
			Actions builder = new Actions(driver);
			Actions hoverOverRegistrar = builder.moveToElement(myElement);
			hoverOverRegistrar.perform();
		} catch (Exception e) {
			return "Fail not able to hover over the element" + " " + xpathKey;
		}
		// sleep(5);
		return "pass";
	}

	private String implicitWait() {
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		return "pass";

	}

	public String openBrowser(String BrowserType) {
		System.out.println("Executing open browser function");
		if (config.getProperty(BrowserType).equals("FireFox"))
			driver= new FirefoxDriver();

		else if (config.getProperty(BrowserType).equals("Chrome")) {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir")
							+ "\\Resources\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (config.getProperty(BrowserType).equals("IE"))
			driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		return "pass";
	}

	public String navigate(String URLKey) {
		System.out.println("Executing navigation function");
		try {
			driver.get(config.getProperty(URLKey));
		} catch (Exception e) {
			return "Fail - not able to navigate";
		}
		return "pass";
	}

	public String click(String xpathKey) {
		System.out.println("Executing click");
		try {
			driver.findElement(By.xpath(Objects.getProperty(xpathKey))).click();
		} catch (Exception e) {
			return "Fail not able to click over the element" + " " + xpathKey;
		}
		return "pass";
	}

	public String IsElementPresent(String xpathKey) {
		System.out.println("Checking if the element is present");
		List<WebElement> output = driver.findElements(By.xpath(Objects
				.getProperty(xpathKey)));
		int a = output.size();
		System.out.println("The number of elements found is " + a);
		if (a != 0) {
			System.out.println("element is present");
			return "pass";
		} else {
			System.out.println("elements not found");
			return "The element is not present" + xpathKey;
		}

	}

	public String getCurrentUrl() {
		return driver.getCurrentUrl();
	}

	public static String getTitle() {
		return driver.getTitle();
	}

	private enum Browser {
		IE, FF, CHROME;
	}

	public String Pageloadtimeout() {
		driver.manage().timeouts().pageLoadTimeout(1000, TimeUnit.SECONDS);
		return "pass";
	}

	public String closeBrowser() {
		driver.close();
		
		driver.quit();
		return "pass";
	}

	private int parseToInt(String number) {
		Double doubleVal = Double.valueOf(number);
		return doubleVal.intValue();
	}

	public void clickOnCategoryNavigationArrow() {
		driver.findElement(By.xpath("//*[@id='topmenu-nav-next']")).click();
	}


}
